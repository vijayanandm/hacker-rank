""" Node is defined as
class node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
"""
INT_MAX = 4294967296
INT_MIN = -4294967296
def minValueNode(node):
    current = node
 
    # loop down to find the leftmost leaf
    while(current.left is not None):
        current = current.left 
 
    return current.data 

def maxValueNode(node):
    current = node
 
    # loop down to find the rightmost leaf
    while(current.right is not None):
        current = current.right 
 
    return current.data

prev = None
setNodes = set()
def countBST(root):
    if root is None:
        return 0
    else:
        return (countBST(root.left) + 1 + countBST(root.right))
    
def uniqueNodesinBST(root):
    global setNodes
    if root is None:
        return 0
    else:
        count = 0
        if(root.data in setNodes):
            return (uniqueNodesinBST(root.left) + count + uniqueNodesinBST(root.right))
        else:
            count = 1
            setNodes.add(root.data)
    return (uniqueNodesinBST(root.left) + count + uniqueNodesinBST(root.right))

def isDistinct(root):
    nodes = countBST(root)
    unique = uniqueNodesinBST(root)
#    print("nodes = {}".format(nodes))
#    print("unique = {}".format(unique))
    if nodes==unique:
        return True
    else:
        return False
    
def checkBST(root):
    # prev is a global variable
    global prev
    prev = None
#    print(isDistinct(root))
    return isbst_rec(root) and isDistinct(root)


def isBSTUtil(node, mini, maxi):

    # An empty tree is BST
    if node is None:
        return 'Yes'
 
    # False if this node violates min/max constraint
    if node.data < mini or node.data > maxi:
        return 'No'
 
    # Otherwise check the subtrees recursively
    # tightening the min or max constraint
    return (isBSTUtil(node.left, mini, node.data -1) and
          isBSTUtil(node.right, node.data+1, maxi))

def isbst_rec(root):
     
    # prev is a global variable
    global prev 
 
    # if tree is empty return true
    if root is None:
        return True
 
    if isbst_rec(root.left) is False:
        return False
 
    # if previous node'data is found 
    # greater than the current node's
    # data return fals
    if prev is not None and prev.data > root.data:
        return False
 
    # store the current node in prev
    prev = root
    return isbst_rec(root.right)

