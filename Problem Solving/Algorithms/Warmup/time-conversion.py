#!/bin/python3

import os
import sys

#
# Complete the timeConversion function below.
#
def timeConversion(s):
    #
    # Write your code here.
    #
    format=s[-2:]
    time = s[:8]
    hour = time[:2]

    if(format == 'PM'):
        if(int(hour) != 12):
            hour24 = int(hour) + 12
            time = str(hour24) + time[2:8]
    else:
        if(int(hour) == 12):
            time = '00' + time[2:8]
    return(time)

if __name__ == '__main__':
    f = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = timeConversion(s)

    f.write(result + '\n')

    f.close()
